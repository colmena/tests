#include <signal.h>		// SIGINT
#include <iostream>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/program_options.hpp>
#include <Colmena/network/NetworkServer.hpp>
#include <Colmena/network/AppMessage/Serializer.hpp>

#define PORT 8000

NetworkServer* srv_ptr;
std::map<size_t, std::list<std::string> > res;

void interrupt_signal(int s)
{
  std::cerr << "interrupt catched, shutting down" << std::endl;
  
  std::cerr << "resultado" << std::endl;
	forall(it, res)
	{
		std::cerr << "	" << it->first << ": ";
		forall(it2, it->second)
			std::cerr << *it2 << " ";
		std::cerr << std::endl;
	}

	srv_ptr->stop();
}

class Notifier
{
    public:
        void NotifyMsg(clientId client_id, const NetworkMessage& msg);
        void NotifyConnection(clientId client_id);
        void NotifyDisconnection(clientId client_id);
};

void Notifier::NotifyMsg(clientId client_id, const NetworkMessage& msg)
{
	size_t id;
	std::string str;

	const uint8_t* buff = msg.GetBody();
	buff = serialization::Serializer<size_t>::Deserialize(buff, id);
	buff = serialization::Serializer<std::string>::Deserialize(buff, str);

	res[id].push_back(str);

  printf("recieved from %lu: '%s' \n", id, str.c_str());
}

void Notifier::NotifyConnection(clientId client_id)
{
    printf("Client %i connected\n", client_id);
}

void Notifier::NotifyDisconnection(clientId client_id)
{
    printf("Client %i disconnected\n", client_id);
}

int main(int argc, char **argv)
{
	// parse program options
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("expect", boost::program_options::value<int>(), "number of expected clients")
	;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);    

	if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
	}

	int expected = -1;
	if (vm.count("expect")) {
     expected = vm["expect"].as<int>();
	} else {
    std::cout << "Number of expected clients unset.\n";
	}

	// initialize server
	boost::asio::io_service io_service;
	boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), PORT);
	srv_ptr = new NetworkServer(io_service, endpoint);
	NetworkServer& server = *srv_ptr;

	// subscribe a notifier
	Notifier notifier;
	server.OnNewMessage().Subscribe(&notifier, &Notifier::NotifyMsg);
	server.OnClientConnected().Subscribe(&notifier, &Notifier::NotifyConnection);
	server.OnClientDisconnected().Subscribe(&notifier, &Notifier::NotifyDisconnection);

	// catch SIGINT signal to properly deactivate the sensor
	//signal(SIGABRT, &interrupt_signal);
	signal(SIGINT, &interrupt_signal);

	// start server
	server.start();

	return 0;
}
