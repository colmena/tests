
#include <boost/program_options.hpp>
#include <Colmena/Event/Event.hpp>
#include <Colmena/network/ClientSocket.hpp>
#include <Colmena/network/AppMessage/Serializer.hpp>

#define PORT 8000

int id;
ClientSocket client;

class Notifier
{
    public:

        void Notify(int server_id, NetworkMessage* msg);
};

void Notifier::Notify(int server_id, NetworkMessage* msg)
{
    printf("recieved msg!");
}

void sendMsg(const std::string& s)
{
	size_t len = serialization::Serializer<std::string>::GetLength(s) + serialization::Serializer<size_t>::GetLength(id);

	// Convert to NetworkMessage and send
	NetworkMessage net_msg( len );

	uint8_t* buff = net_msg.GetBody();
	buff = serialization::Serializer<size_t>::Serialize(buff, id);
	buff = serialization::Serializer<std::string>::Serialize(buff, s);

	client.Send( net_msg );
}

int main(int argc, char **argv)
{
	// parse program options
	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("id", boost::program_options::value<int>(), "client id")
	;

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
	}

	if (vm.count("id")) {
		id = vm["id"].as<int>();
		std::cout << "id: " << id << "\n";
     
	} else {
		std::cout << "id unset.\n";
    return 1;
	}

	// start client

	client.Connect("localhost", 8000);

	// hex("hola?") = 68 6f 6c 61 3f 00
	sendMsg("hola");
	sendMsg("que tal");
	sendMsg("?");

	Notifier notifier;
	ScopedSubscription subscription;

	subscription = client.OnNewMessage().Subscribe(&notifier, &Notifier::Notify);
	
	sleep(2);
	
	return 0;
}
